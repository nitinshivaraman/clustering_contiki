#!/bin/bash

for i in {1..100}
do
	ant run_nogui -Dargs=/home/nitin.shivaraman/contiki/200_100m/200_$i.csc
	cd build
	mv COOJA.testlog power/200_BEEM_$i.testlog
	rm COOJA.log
	cd ..
done
