#define NETSTACK_CONF_RADIO cc2420_driver
#define NETSTACK_CONF_FRAMER framer_802154
#define NETSTACK_CONF_RDC  cxmac_driver  // nullrdc_driver
#define NETSTACK_CONF_RDC_CHANNEL_CHECK_RATE 32
#define NETSTACK_CONF_MAC  csma_driver

//#undef TIMESYNCH_CONF_ENABLED
//#define TIMESYNCH_CONF_ENABLED 1
