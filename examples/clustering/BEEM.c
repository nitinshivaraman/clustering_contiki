/*
 * Copyright (c) 2007, Swedish Institute of Computer Science.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

/**
 * \file
 *         BEEM protocol for Contiki
 * \author
 *         Nitin Shivaraman <nitin.shivaraman@tum-create.edu.sg>
 */

#include "contiki.h"
#include "lib/random.h"
#include "net/rime.h"
#include "dev/cc2420.h"
#include "dev/watchdog.h"
#include "lib/list.h"
#include "lib/memb.h"
#include "sys/rtimer.h"
#include "node-id.h"
#include "powertrace.h"
#include "net/netstack.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define INIT_ENERGY                2000   // The initial energy of all the nodes in mAh
#define NUM_NODES                  50     // Total number of nodes
#define INIT_PROB                  5     // The initial set of Cluster heads
#define RSSI_OFFSET                -45    // As mentioned in the datasheet of cc2420
#define FALSE                      0      // Error state
#define TRUE                       1      // OK state
#define MAX_NEIGHBOURS             64     // Maximum number of neighbour nodes for each node
#define BROADCAST_CHANNEL          7      // Channel used for broadcast data transfer
#define UNICAST_CHANNEL            146    // Channel used for unicast data transfer
#define ROUND_LIMIT                1      // Number of rounds each state would last
#define EPOCH                      10     // Number of rounds per epoch
#define PMIN                       0.001 // Minimum probability 
#define ROUND_TIME                 1.1    // The time required for a round: 0.8 for 50 nodes, 1.1 for 100 nodes and 2.2 for 200 nodes
#define MIN_INTERVAL CLOCK_SECOND * ROUND_TIME
#define max(x,y) ((x) >= (y)) ? (x) : (y)
#define min(x,y) ((x) <= (y)) ? (x) : (y)


/**
 *  Message format
 */
typedef struct message_t
{
    uint16_t id;                        /// << ID of the sender
    uint16_t state;                     /// << State of the node (normal/CH)
    uint16_t num_neighbours;                   /// << Request a CH for cluster membership
    //uint16_t root;                      /// << ID of root of the sender
}
message_t;


/**
 *  Enum for state transitions
 */
typedef enum
{
    NEIGHBOUR_DISCOVERY,
    CH_ELECTION,
    CLUSTER_FORMATION,
    STEADY_STATE
} state;


/**
 *  Enum for node status
 */
typedef enum
{
    NON_CH,
    TENTATIVE_CH,
    FINAL_CH,
    DEAD
} node_state;


/**
 *  Neighbour array
 */
struct neighbour {
  struct    neighbour *next;     /// << The ->next pointer
  uint16_t  id;                  /// << The ID of the neighbour 
  uint16_t  state;               /// << Root of the neighbour
  uint16_t  num_neighbours;      /// << Number of neighbours of the neighbour
};

LIST(neighbour_list);
MEMB(neighbour_memb, struct neighbour, MAX_NEIGHBOURS);

static uint8_t  epoch_round = 0;
//static float    p = 0.1;
static uint8_t  final_CH_cost = 0;
static node_state CH_state = NON_CH;
static uint8_t  CH_covered_final = FALSE, CH_covered = FALSE;
static uint16_t root_id = 0;
static uint8_t  sub_round = -1;
static uint8_t  broadcast_msg = 1;
static uint8_t  unicast_msg = 0;
static uint8_t  isolated_node = 1;
static state    mode = NEIGHBOUR_DISCOVERY;
static uint8_t  num_neighbours = 0, add_round = 0;
uint8_t neighbour_phase = 1;
void add_to_neighbour(uint16_t , uint16_t , int16_t , uint8_t );

PROCESS(beem_process, "BEEM Clustering process");
AUTOSTART_PROCESSES(&beem_process);


/*---------------------------------------------------------------------------*/
static void
broadcast_recv(struct broadcast_conn *c, const rimeaddr_t *from)
{
    // synchronization message
    message_t syncMsg;
    int16_t received_rssi = cc2420_last_rssi;
    received_rssi += RSSI_OFFSET;
    
    
    /* The packetbuf_dataptr() returns a pointer to the first data byte
       in the received packet. */
    
    memcpy(&syncMsg, packetbuf_dataptr(), sizeof(struct message_t));
    //printf("Received RSSI is %d from node %d\n", received_rssi, syncMsg.id);
    // Nitin: Transmission window is 50m
    if((syncMsg.id < NUM_NODES) && (received_rssi >= -55))
    {
        if((syncMsg.state >= 0) && (syncMsg.state < 4))
            add_to_neighbour(syncMsg.id, syncMsg.state, syncMsg.num_neighbours, FALSE);
        else
            add_to_neighbour(syncMsg.id, 0, (syncMsg.num_neighbours & 0xFF), FALSE);
    }

    if(mode == STEADY_STATE)
    {
        if((syncMsg.state == FINAL_CH) && (CH_state == FINAL_CH))
        {
            printf("CH neighbour of %d is %d\n", node_id, syncMsg.id);
        }
    }
}


void add_to_neighbour(uint16_t msg_id, uint16_t msg_state, int16_t no_neighbours, uint8_t remove)
{
    struct neighbour *n;
    isolated_node = 0;

    //printf("RSSI in add_to_neighbour is %d\n", rssi);
    /* Check if we already know this neighbor. */
    for(n = list_head(neighbour_list); n != NULL; n = list_item_next(n)) 
    {
        if(n->id == msg_id)
        {
            if((remove))
            {
                list_remove(neighbour_list, n);
                memb_free(&neighbour_memb, n);
            }
            else
            {
                //printf("Setting state1 to %d for node %d\n", msg_state, msg_id);
                n->state = msg_state;
                n->num_neighbours = no_neighbours;
                if(no_neighbours < num_neighbours)
                {
                    final_CH_cost = no_neighbours;
                }
                if(n->state == FINAL_CH)
                {
                    CH_covered = TRUE;
                    CH_covered_final = TRUE;
                }
                else if(n->state == TENTATIVE_CH)
                    CH_covered = TRUE;
            }
            return;
        }
    }

    if(n == NULL) 
    {
        n = memb_alloc(&neighbour_memb);

        /* If we could not allocate a new neighbor entry, we give up */
        if(n == NULL) 
          return;
        n->id = msg_id;
        n->state = msg_state;
        n->num_neighbours = no_neighbours;
        if(no_neighbours < num_neighbours)
        {
            final_CH_cost = no_neighbours;
        }
        //printf("Setting state2 to %d for node %d\n", msg_state, msg_id);
        //cluster_neighbours++;
        
        if(n->state == FINAL_CH)
        {
            CH_covered = TRUE;
            CH_covered_final = TRUE;
        }
        else if(n->state == TENTATIVE_CH)
            CH_covered = TRUE;
        

        list_add(neighbour_list, n);
        num_neighbours++;

    }

}

   
 void elect_CH(void)
 {
    float p = 0; //T, s;
    uint16_t residual = 0;
    static float energy_prob = 0, AverageDensity = 0;
    uint8_t C1 = 0, C2 = 0, C3 = 0;
    p = INIT_PROB;
    // TODO: NITIN: Compute cost of each node based on their neighbours cost
    epoch_round++;
    
    C1 = ((0.1*(abs(rand()) % 1000)) <= p);
    if(!add_round)
    {
        residual = powertrace_getresidual() - 5;
        energy_prob = (residual / INIT_ENERGY);
        //AverageDensity = NodeNums / 100 * 100 * 25 * 25 * 3.14;   // 100x100m area with 50m transmission range
        AverageDensity = (NUM_NODES / 4) * 3.14;   // 100x100m area with 50m transmission range
    }

    C2 = (max(energy_prob,PMIN) >= 1);
    C3 = ((num_neighbours / AverageDensity) >= 1);

    //if ((C2 && C3) || (C3 && C1) || (C1 && C2))
    if((C2==1) || (C3 ==1))
    {
        root_id = node_id;
        CH_state = FINAL_CH;
        final_CH_cost = num_neighbours;
        return;
    }
    else if (C1 == 1)
    {
        root_id = node_id;
        CH_state = TENTATIVE_CH;
        final_CH_cost = num_neighbours;
    }

    energy_prob = min((energy_prob*2), 1);
    AverageDensity = min((AverageDensity*2), 1);
    C2 = (max(energy_prob,PMIN) >= 1);
    C3 = ((num_neighbours / AverageDensity) >= 1);

    if((C2==1)|| (C3 ==1))
    {

        add_round++;
        //sub_round--;
        mode = CLUSTER_FORMATION;
    }
 
}

void choose_cluster(void)
{
    //int16_t lowest_rssi = -180;
    struct neighbour *n;
    if ((CH_state != FINAL_CH) && (CH_covered_final))
    {
        // Select the root node for the non-CH nodes.
        for(n = list_head(neighbour_list); n != NULL; n = list_item_next(n)) 
        {
            if((n->state == FINAL_CH))
            {
                CH_state = NON_CH;
                root_id = n->id;
                final_CH_cost = n->num_neighbours;
            }
        }
    }
    else
    {
       root_id = node_id;
       CH_state = FINAL_CH;
       final_CH_cost= num_neighbours;
    }

    printf("Setting root for node %d as %d\n", node_id, root_id);
}


static void reset_status()
{
    struct neighbour *n;
    for(n = list_head(neighbour_list); n != NULL; n = list_item_next(n)) 
        n = NULL; //n->state = FALSE;
}


static void state_machine()
{
    sub_round++;
   

    /* 1 sub round is needed for the node start up 
       Elect the node that are CH and broadcast messages */
    if(!(sub_round % (ROUND_LIMIT)) && (mode == NEIGHBOUR_DISCOVERY))
    {
        mode = CH_ELECTION;
        neighbour_phase = 1;
        broadcast_msg = 1;
        //printf("Changing to elect state\n");
        //NETSTACK_RDC.on();
    }
    else if (mode == CH_ELECTION)
    {
        if(isolated_node)
        {
            root_id = node_id;
            CH_state = FINAL_CH;
        }
        elect_CH();
        broadcast_msg = 1;
        neighbour_phase = 1;
        if(CH_state == FINAL_CH)
        {
            mode = STEADY_STATE;
            powertrace_print("");
            printf("Changing to STEADY_STATE state\n");
        }
    }
    else if(mode == CLUSTER_FORMATION)
    {
        choose_cluster();
        mode = STEADY_STATE;
        printf("Changing to STEADY_STATE state\n");
		powertrace_print("");
        if(CH_state != FINAL_CH)
        {
            if(root_id == 0)
                mode = CLUSTER_FORMATION;
            neighbour_phase = 0;
            unicast_msg = 1;
            NETSTACK_RDC.off(0);
        }

    }
    else if(mode == STEADY_STATE)
    {
        if(!(sub_round % EPOCH))
        {
            // Add neighbor_phase and steady state conditions here
            mode = NEIGHBOUR_DISCOVERY;
            neighbour_phase = 1;
            reset_status();
            NETSTACK_RDC.on();
            sub_round = 0;
            //sub_round++;
            unicast_msg = 0;
            CH_covered_final = FALSE;
            CH_covered = FALSE;
            root_id = 0;
            add_round = 0;
            final_CH_cost = 0;
            CH_state = NON_CH;
            powertrace_print("");
            //printf("Changing to elect state\n");
        }
    }

    printf("Cluster head for node %d is node %d\n", node_id, root_id);

}


/* This function is called for every incoming unicast packet. */
static void
recv_uc(struct unicast_conn *c, const rimeaddr_t *from)
{
    message_t syncMsg;
    int16_t received_rssi = cc2420_last_rssi;
    received_rssi += RSSI_OFFSET;
    memcpy(&syncMsg, packetbuf_dataptr(), sizeof(struct message_t));
    //printf("Received unicast msg from node %d\n", from->u8[0]);
    
}

static const struct broadcast_callbacks broadcast_call = {broadcast_recv};
static const struct unicast_callbacks unicast_callbacks = {recv_uc};
static struct broadcast_conn broadcast;
static struct unicast_conn unicast;
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(beem_process, ev, data)
{
  static struct etimer sendtimer;
  static clock_time_t interval;
  message_t beacon;
  message_t unicast_beacon;
  rimeaddr_t send_addr;
  
  uint16_t seconds=10;// warning: if this variable is changed, then the kinect variable the count the minutes should be changed
  double fixed_perc_energy = 0.002;// 0 - 1
  uint16_t variation = 2;//0 - 99
  static uint16_t residual = 0;

  PROCESS_EXITHANDLER(broadcast_close(&broadcast);)

  PROCESS_BEGIN();

  /* Initialize the memory for the neighbour entries. */
  memb_init(&neighbour_memb);
  list_init(neighbour_list);

  powertrace_start(CLOCK_SECOND * seconds, seconds, fixed_perc_energy, variation);
  //powertrace_start(CLOCK_SECOND * 10);

  unicast_open(&unicast, UNICAST_CHANNEL, &unicast_callbacks);

  broadcast_open(&broadcast, BROADCAST_CHANNEL, &broadcast_call);
  // node_id_burn(node_id); // To be used for hardware

  interval = MIN_INTERVAL;

  while(1)
  {
    etimer_set(&sendtimer, interval);

    PROCESS_WAIT_UNTIL(etimer_expired(&sendtimer));

    residual = powertrace_getresidual();

    if(!residual)
    {
        printf("Residual energy of node %d is 0\n", node_id);
        CH_state = DEAD;
        break;
    }

    state_machine();
    //printf("Sending id as %d with root as %d\n", node_id, root_id);

    // Send a broadcast if node is a CH
    if((CH_state == (FINAL_CH)) || (mode == (NEIGHBOUR_DISCOVERY || CH_ELECTION)))
    {
        beacon.id = node_id;
        //printf("Sending id as %d with root as %d\n", beacon.id, root_id);
        beacon.state = CH_state;
        beacon.num_neighbours = num_neighbours;
        packetbuf_copyfrom(&beacon, sizeof(beacon));
        broadcast_send(&broadcast);
    }
    // send a message to root for cluster membership with unicast
  if((mode == STEADY_STATE) && (CH_state != FINAL_CH) && (unicast_msg))
    {
        beacon.id = node_id;
        beacon.state = CH_state;
        //printf("CH state should be %d\n", CH_state);
        beacon.num_neighbours = num_neighbours;
        send_addr.u8[0]=root_id;
        send_addr.u8[1]=0;
        //printf("Sending unicast from %d to %d\n", beacon.id, send_addr.u8[0]);
        packetbuf_copyfrom(&unicast_beacon, sizeof(unicast_beacon));
        unicast_send(&unicast, &send_addr);
    }

    // PROCESS_WAIT_UNTIL(etimer_expired(&intervaltimer));
    if(CH_state == DEAD)
        break;

    PROCESS_END();
  }
  return 1;
}
