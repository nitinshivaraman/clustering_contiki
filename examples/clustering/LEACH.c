/*
 * Copyright (c) 2007, Swedish Institute of Computer Science.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

/**
 * \file
 *         LEACH protocol for Contiki
 * \author
 *         Nitin Shivaraman <nitin.shivaraman@tum-create.edu.sg>
 */

#include "contiki.h"
#include "lib/random.h"
#include "net/rime.h"
#include "dev/cc2420.h"
#include "dev/watchdog.h"
#include "lib/list.h"
#include "lib/memb.h"
#include "sys/rtimer.h"
#include "node-id.h"
#include "powertrace.h"
#include "net/netstack.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


// TODO: Change naming of IsCurrentCH

#define INIT_PROB                  10     // The initial % set of Cluster heads
#define RSSI_OFFSET                -45    // As mentioned in the datasheet of cc2420
#define FALSE                      0      // Error state
#define TRUE                       1      // OK state
#define CH_NODE                    1      // Node is a CH
#define MAX_NEIGHBOURS             64     // Maximum number of neighbour nodes for each node
#define BROADCAST_CHANNEL          7      // Channel used for broadcast data transfer
#define UNICAST_CHANNEL            146    // Channel used for unicast data transfer
#define ROUND_LIMIT                1      // Number of rounds each state would last
#define EPOCH                      10      // Number of rounds per epoch
#define ROUND_TIME                 1.1    // The time required for a round: 0.8 for 50 nodes, 1.1 for 100 nodes and 2.2 for 200 nodes
#define MIN_INTERVAL CLOCK_SECOND * ROUND_TIME


/**
 *  Message format
 */
typedef struct message_t
{
    uint16_t id;                        /// << ID of the sender
    uint16_t state;                     /// << State of the node (normal/CH)
    uint16_t request;                   /// << Request a CH for cluster membership
    //uint16_t root;                      /// << ID of root of the sender
}
message_t;


/**
 *  Enum for state transitions
 */
typedef enum
{
    NEIGHBOUR_DISCOVERY,
    CH_ELECTION,
    CLUSTER_FORMATION,
    STEADY_STATE
} state;


/**
 *  Neighbour array
 */
struct neighbour {
  struct    neighbour *next;     /// << The ->next pointer
  uint16_t  id;                  /// << The ID of the neighbour 
  uint16_t  state;               /// << Root of the neighbour
  uint16_t  request;             /// << Request from leaf for cluster membership
  int16_t   rssi;                /// << Reset counter if the node fails
};

LIST(neighbour_list);
MEMB(neighbour_memb, struct neighbour, MAX_NEIGHBOURS);

static uint8_t  epoch_round = 0;
static float    p = 0.1;
static uint8_t  IsCurrentCH = 0;
static uint16_t root_id = 0;
static uint8_t  sub_round = 0;
static uint8_t  broadcast_msg = 1;
static uint8_t  unicast_msg = 0;
static uint8_t  isolated_node = 1;
static uint8_t  mode = NEIGHBOUR_DISCOVERY;
uint8_t neighbour_phase = 1;
void add_to_neighbour(uint16_t , uint16_t , int16_t , uint8_t );

PROCESS(leach_process, "LEACH Clustering process");
AUTOSTART_PROCESSES(&leach_process);

/*---------------------------------------------------------------------------*/
static void
broadcast_recv(struct broadcast_conn *c, const rimeaddr_t *from)
{
    // synchronization message
    message_t syncMsg;
    int16_t received_rssi = cc2420_last_rssi;
    received_rssi += RSSI_OFFSET;
    
    
    /* The packetbuf_dataptr() returns a pointer to the first data byte
       in the received packet. */
    //printf("Received RSSI is %d\n", received_rssi);
    memcpy(&syncMsg, packetbuf_dataptr(), sizeof(struct message_t));
    //printf("Received broadcast msg from node %d and its state is %d\n", syncMsg.id, syncMsg.state);

    add_to_neighbour(syncMsg.id, syncMsg.state, received_rssi, FALSE);

    if(mode == STEADY_STATE)
    {
        if((syncMsg.state == TRUE) && (IsCurrentCH == TRUE))
        {
            printf("CH neighbour of %d is %d\n", node_id, syncMsg.id);
        }
    }
}


void add_to_neighbour(uint16_t msg_id, uint16_t msg_state, int16_t rssi, uint8_t remove)
{
    struct neighbour *n;
    isolated_node = 0;

    /* Check if we already know this neighbor. */
    for(n = list_head(neighbour_list); n != NULL; n = list_item_next(n)) 
    {
        if(n->id == msg_id)
        {
            if((remove))
            {
                list_remove(neighbour_list, n);
                memb_free(&neighbour_memb, n);
            }
            else
            {
                //printf("Setting state1 to %d for node %d\n", msg_state, msg_id);
                n->state = msg_state;
                n->rssi = rssi;
            }
            return;
        }
    }

    if(n == NULL) 
    {
        n = memb_alloc(&neighbour_memb);

        /* If we could not allocate a new neighbor entry, we give up */
        if(n == NULL) 
          return;
        n->id = msg_id;
        n->state = msg_state;
        n->rssi = rssi;

        list_add(neighbour_list, n);

    }

}

   
 void elect_CH(void)
 {
    float T, s;
    uint16_t residual = 0;
    //p = INIT_PROB * 0.01;

    epoch_round++;

    residual = powertrace_getresidual();
     
    if(residual > 100)
    {
        // Calculate the value of T for each round
        T = p / (1 - (p * (epoch_round % (uint8_t)(1/p))));

        // If the node has not yet become a cluster head
        // Each node produces a random number
        s = 0.001*(abs(rand()) % 1000); //(float)rand() / RAND_MAX;
        if (s <= T)
        {
            // Elect the cluster head
            IsCurrentCH = TRUE;
            root_id = node_id;
            printf("CH is set to node %d\n", node_id);
        }
        else
        {
            IsCurrentCH = FALSE;     
        } 
         
    }
    else
    {
        IsCurrentCH = FALSE;
    }
 
}

void choose_CH(void)
{
    int16_t lowest_rssi = -180;
    struct neighbour *n;
    // Select the root node for the non-CH nodes.
    for(n = list_head(neighbour_list); n != NULL; n = list_item_next(n)) 
    {
        if((n->state == CH_NODE) && (!IsCurrentCH))
        {
            // Elect the CH which is nearest; RSSI is -ve, hence >
            //printf("The RSSI value is %d\n", n->rssi);
            if(n->rssi > lowest_rssi)
            {
                lowest_rssi = n->rssi;
                root_id = n->id;
                IsCurrentCH = FALSE;
            }
        }
    }

    printf("Setting root for node %d as %d\n", node_id, root_id);
}


static void reset_status()
{
	struct neighbour *n;
    for(n = list_head(neighbour_list); n != NULL; n = list_item_next(n)) 
    	n->state = FALSE;
}


static void state_machine()
{
    // Isolated node without any neighbours
    if(isolated_node)
    {
        root_id = node_id;
        IsCurrentCH = TRUE;
    }
    
    sub_round++;

    /* 1 sub round is needed for the node start up 
       Elect the node that are CH and broadcast messages */
    if(!(sub_round % (ROUND_LIMIT)) && (mode == NEIGHBOUR_DISCOVERY))
    {
        mode = CH_ELECTION;
        neighbour_phase = 1;
        broadcast_msg = 0;
        //printf("Changing to elect state\n");
        //NETSTACK_RDC.on();
    }
    else if(!(sub_round % (1 + ROUND_LIMIT)) && (mode == CH_ELECTION))
    {
        mode = CLUSTER_FORMATION;
        elect_CH();
        neighbour_phase = 1;
    }
    /* Nodes that are non-CH choose one among existing CHs */
    else if((!(sub_round % (2 + ROUND_LIMIT)) && (mode == CLUSTER_FORMATION)))
    {
        mode = STEADY_STATE;
        if(!IsCurrentCH)
        {
            choose_CH();
            broadcast_msg = 0;
            unicast_msg = 1;
            neighbour_phase = 0;
            NETSTACK_RDC.off(0);
        }
        else
            NETSTACK_RDC.on();
        printf("Changing to STEADY_STATE state\n");
        powertrace_print("");
        
    }


    
    if(!(sub_round % EPOCH) && (mode == STEADY_STATE))
    {
        // Add neighbor_phase and steady state conditions here
        //printf("Cluster head for node %d is node %d\n", node_id, root_id);
        mode = CH_ELECTION;
        neighbour_phase = 1;
        reset_status();
        NETSTACK_RDC.on();
        sub_round = 0;
        sub_round++;
        unicast_msg = 0;
        powertrace_print("");
        //printf("Changing to elect state\n");
    }

    printf("Cluster head for node %d is node %d\n", node_id, root_id);
}


/* This function is called for every incoming unicast packet. */
static void
recv_uc(struct unicast_conn *c, const rimeaddr_t *from)
{
    message_t syncMsg;
    int16_t received_rssi = cc2420_last_rssi;
    received_rssi += RSSI_OFFSET;
    memcpy(&syncMsg, packetbuf_dataptr(), sizeof(struct message_t));
    //printf("Received unicast msg from node %d\n", from->u8[0]);
    
    add_to_neighbour(syncMsg.id, syncMsg.state, received_rssi, FALSE);
}

static const struct broadcast_callbacks broadcast_call = {broadcast_recv};
static const struct unicast_callbacks unicast_callbacks = {recv_uc};
static struct broadcast_conn broadcast;
static struct unicast_conn unicast;
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(leach_process, ev, data)
{
  static struct etimer sendtimer;
  static clock_time_t interval;
  message_t beacon;
  //static uint8_t advertisement = 0;
  message_t unicast_beacon;
  rimeaddr_t send_addr;
  
  uint16_t seconds=100;// warning: if this variable is changed, then the kinect variable the count the minutes should be changed
  double fixed_perc_energy = 0.002;// 0 - 1
  uint16_t variation = 2;//0 - 99
  static uint16_t residual = 0;

  PROCESS_EXITHANDLER(broadcast_close(&broadcast);)

  PROCESS_BEGIN();

  /* Initialize the memory for the neighbour entries. */
  memb_init(&neighbour_memb);
  list_init(neighbour_list);

  powertrace_start(CLOCK_SECOND * seconds, seconds, fixed_perc_energy, variation);
  //powertrace_start(CLOCK_SECOND * 10);

  unicast_open(&unicast, UNICAST_CHANNEL, &unicast_callbacks);

  broadcast_open(&broadcast, BROADCAST_CHANNEL, &broadcast_call);
  // node_id_burn(node_id); // To be used for hardware

  interval = MIN_INTERVAL;

  while(1)
  {
    etimer_set(&sendtimer, interval);

    PROCESS_WAIT_UNTIL(etimer_expired(&sendtimer));

    residual = powertrace_getresidual();

    if(!residual)
    {
        printf("Residual energy of node %d is 0\n", node_id);
        break;
    }

    state_machine();

    // Send a broadcast if node is a CH
    if(broadcast_msg || IsCurrentCH)
    {
        beacon.id = node_id;
        beacon.state = IsCurrentCH;
        packetbuf_copyfrom(&beacon, sizeof(beacon));
        //printf("Sending broadcast from %d\n", beacon.id);
        broadcast_send(&broadcast);
    }
    // send a message to root for cluster membership with unicast
    else if(((mode == STEADY_STATE) || unicast_msg) && !IsCurrentCH)
    {
        beacon.id = node_id;
        beacon.state = IsCurrentCH;
        send_addr.u8[0]=root_id;
        send_addr.u8[1]=0;
        //printf("Sending unicast from %d to %d\n", beacon.id, send_addr.u8[0]);
        packetbuf_copyfrom(&unicast_beacon, sizeof(unicast_beacon));
        unicast_send(&unicast, &send_addr);
    }

    PROCESS_END();
  }
  return 1;
}
