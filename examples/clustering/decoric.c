/*
 * Copyright (c) 2007, Swedish Institute of Computer Science.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

/**
 * \file
 *        Connected and Resilient Clustering in Contiki
 * \author
 *         Nitin Shivaraman <nitin.shivaraman@tum-create.edu.sg>
 */

#include "contiki.h"
#include "lib/random.h"
#include "net/rime.h"
#include "dev/cc2420.h"
#include "lib/list.h"
#include "lib/memb.h"
#include "sys/rtimer.h"
#include "node-id.h"
#include "powertrace.h"
#include "net/netstack.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#define FIXED_ROOT_TIMEOUT         3      // number of iterations after root is fixed to stop transmission
#define ERROR_VALID_THRESHOLD      3      // Threshold of error from consensus to initiate sleep
#define CYCLE_LIMIT                6      // Number of rounds for a non-CH to transmit
#define NODE_FAIL_LIMIT            6      // Number of cycles to know if there a node failure
#define ROUND_LIMIT                1      // Number of seconds/transmission window for all nodes to trasmit atleast once
#define FALSE                      0      // Error state
#define TRUE                       1      // OK state
#define PROTOCOL_DISPATCH          0x01   // Identifier for the protocol
#define UNKNOWN_NODE               0xFF   // Default value of a node
#define RSSI_THRESHOLD             65     // Signal strength for neighbours
#define ROOT_RESET_LIMIT           5      // Iterations to wait if there is a root failure
#define RSSI_OFFSET                -45    // As mentioned in the datasheet of cc2420
#define MAX_NEIGHBOURS             64     // Maximum number of neighbour nodes for each node
#define MAX_ACTIVE                 36     // Maximum number of nodes that is tracked for active nodes
#define XMISSION_ROUND             1      // Number of rounds each node can complete transmission
#define XFER_CHANNEL               7      // Channel used for data transfer
#define EPOCH                      10     // Number of rounds per epoch
#define ROUND_TIME                 1.1    // The time required for a round: 0.8 for 50 nodes, 1.1 for 100 nodes and 2.2 for 200 nodes
// A message is sent every 1.1 seconds by the root
#define MIN_INTERVAL CLOCK_SECOND * ROUND_TIME

/**
 *  Message format
 */
struct message_t
{
    uint16_t  dispatch_marker;           /// << protocol marker
    uint16_t  id;                        /// << ID of the sender
    uint16_t  root;                      /// << ID of root of the sender
    uint16_t  neighbours;                /// << Degree of the node
    uint16_t  new_root;                  /// << This node is the new CH
    uint8_t   gossip_list[MAX_ACTIVE];   /// << The list of neighbours of the node
    
};


/**
 *  Enum for state transitions
 */
typedef enum
{
    NEIGHBOUR_DISCOVERY,
    CH_ELECTION,
    CLUSTER_CORRECTION,
    HEALTH_UPDATE
} state;


/**
 *  Neighbour array
 */
struct neighbour {
  struct    neighbour *next;     /// << The ->next pointer
  uint16_t  id;                  /// << The ID of the neighbour 
  uint16_t  root;                /// << Root of the neighbour
  uint8_t   reset;               /// << Reset counter if the node fails
  uint32_t  ext_neighb;          /// << Identifier for external neighbour
};

LIST(neighbour_list);
MEMB(neighbour_memb, struct neighbour, MAX_NEIGHBOURS);

/* Since contiki does not have a hierarchy, temporarily self as root */
static uint8_t  root_id = 0;
static uint8_t  external = 0;
static uint16_t cluster_neighbours;
static uint8_t  highest_degree = 0;
static uint8_t  shighest_degree = 0;
static uint8_t  highest_node = 0;
static uint8_t  shighest_node = 0;
static uint8_t  fixed_root = 0;
static uint8_t  detached_needed = 0;
/* Sub-round used as rounds */
static uint8_t  sub_round = 0;
/* Rounds used as cycles */
static uint8_t  rounds = 0;            
static uint8_t  new_root = 0;
static uint8_t  root_gossip = 0;
/* Identify the bridge root that has to become CH */
static uint8_t  detached_root = 0;
/* Choose a random slot within a cycle for non-CH transmission */
static uint8_t  random_tx = 0;
static uint8_t  mode = NEIGHBOUR_DISCOVERY;
static uint8_t  gossip_list[MAX_ACTIVE];
static uint8_t  fixed_neighbours[MAX_ACTIVE];
static uint8_t  root_list[MAX_ACTIVE];
static uint8_t  cluster_members[MAX_ACTIVE];
/* Used to override mac to keep radio always on during neighbour discovery phase */
uint8_t neighbour_phase = 1;



static void state_machine();
/* Verify the neighbor gossip by coorelation of the values in each node's list */
void verify_neighbour_corr(struct message_t *msg);
/* Create an entry for a new neighbor and update the existing neighbor count */
void add_to_neighbour(uint16_t ,uint16_t , uint8_t ext, int16_t ext_neighb, uint8_t remove);

PROCESS(decoric_process, "Clustering process");
AUTOSTART_PROCESSES(&decoric_process);


/*---------------------------------------------------------------------------*/
static void
broadcast_recv(struct broadcast_conn *c, const rimeaddr_t *from)
{
    // synchronization message
    struct message_t syncMsg;
    struct neighbour *n;
    uint8_t position, shift;
    int16_t received_rssi = cc2420_last_rssi;
    received_rssi += RSSI_OFFSET;
    
    
    /* The packetbuf_dataptr() returns a pointer to the first data byte
       in the received packet. */
    //syncMsg = (struct message_t *) packetbuf_dataptr();
    memcpy(&syncMsg, packetbuf_dataptr(), sizeof(struct message_t));
    //printf("Received msg from node %d and its root is %d\n", syncMsg.id, syncMsg.root);
    //printf("Received RSSI is %d\n", received_rssi);
    

    /* Find the highest and second highest neighbour */
    if(abs(received_rssi) <= RSSI_THRESHOLD)
    {
        if(syncMsg.neighbours > highest_degree)
        {
            shighest_degree = highest_degree;
            shighest_node = highest_node;
            highest_degree = syncMsg.neighbours;
            highest_node = syncMsg.id;
        }
        else if((syncMsg.neighbours >= shighest_degree) && (syncMsg.neighbours < highest_degree))
        {
            shighest_degree = syncMsg.neighbours;
            if((syncMsg.neighbours == shighest_degree) && (syncMsg.id < shighest_node))
                shighest_node = syncMsg.id;
        }
        else if((highest_node == syncMsg.id) && syncMsg.new_root)
        {
            root_id = syncMsg.id;
            printf("Received Degree is greater than existing root. Switching to new root\n");
        }

        position = syncMsg.id / 8;
        shift = syncMsg.id % 8;
        if((syncMsg.id == syncMsg.root))
        {
            root_list[position] = root_list[position] | (1U << (shift));
        }
        /* Initially every node will be a root of its own */
        else
        {
            root_list[position] = root_list[position] & ~(1U << (shift));
        }

    }
    
    /* Check if we already know this neighbour and update its count */
    for(n = list_head(neighbour_list); n != NULL; n = list_item_next(n)) 
    {
        position = n->id / 8;
        shift = n->id % 8;
        if(n->id == syncMsg.id)
        {
            n->reset = 0;
            gossip_list[position] = gossip_list[position] | (1U << (shift));
        }
    }

    /* Add neighbour if non-existent in the node's list */
    if((n == NULL) && (mode != NEIGHBOUR_DISCOVERY))
    {
        if(abs(received_rssi) > RSSI_THRESHOLD)
        {
            add_to_neighbour(syncMsg.id, syncMsg.root, TRUE, TRUE, FALSE);
        }
        /* During initial message reception */
        else
        {
            add_to_neighbour(syncMsg.id, syncMsg.root, FALSE, FALSE, FALSE);
        }
    }

    


    /* Clustering starts here */
    switch(mode)
    {
        case NEIGHBOUR_DISCOVERY:
        if(abs(received_rssi) > RSSI_THRESHOLD)
        {
            add_to_neighbour(syncMsg.id, syncMsg.root, TRUE, TRUE, FALSE);
        }
        else
        {
            add_to_neighbour(syncMsg.id, syncMsg.root, FALSE, FALSE, FALSE);
        }
        detached_needed = node_id;
        root_id = node_id;

        break;

        case CH_ELECTION:

        position = syncMsg.id / 8;
        shift = syncMsg.id % 8;

        /* CH is found at the end of this round */
        if((syncMsg.neighbours >= highest_degree) && (abs(received_rssi) <= RSSI_THRESHOLD))
        {
            /* Node has the highest number of neighbours and becomes the head */
            if ((syncMsg.root == syncMsg.id) && (syncMsg.id != root_id))
            {
                if((syncMsg.neighbours > cluster_neighbours))// && (syncMsg->neighbours > highest_degree))
                {
                    if(!detached_root) 
                    {
                        highest_degree = syncMsg.neighbours;
                        root_id = syncMsg.id;
                        fixed_root = 0;
                    }
                }
                /* Another node in the cluster has the same number of neighbours */
                else if ((syncMsg.neighbours == cluster_neighbours))
                {
                    if(!detached_root  && (node_id > syncMsg.id) && (root_id > syncMsg.id))
                    {
                        root_id = syncMsg.id;
                        highest_degree = syncMsg.neighbours;
                        fixed_root = 0;
                    }
                }
            }

            if((syncMsg.id == syncMsg.root))
            {
                root_list[position] = root_list[position] | (1U << (shift));
            }
            /* Initially every node will be a root of its own */
            else
            {
                root_list[position] = root_list[position] & ~(1U << (shift));
            }  
        }

        if(syncMsg.root == root_id)
        {
            cluster_members[position] |= (1U << (shift));
        }

        break;

        case CLUSTER_CORRECTION:

        /* Error Correction: Node claimed itself to be root and later lost its status */
        if((root_id == syncMsg.id) && (syncMsg.id != syncMsg.root))
        {
            root_id = node_id;
            highest_degree = 0;
            fixed_root = 0;
			// Switch back to election phase to get the correct CH
            mode = CH_ELECTION;
            sub_round-=2;
        }

        position = syncMsg.id / 8;
        shift = syncMsg.id % 8;
        if(syncMsg.root == root_id)
        {
            cluster_members[position] |= (1U << (shift));
        }

        /* If there are multiple nodes that need correction, decide whether the current node breaks */
        if((syncMsg.root == root_id) && (syncMsg.id != root_id)) // another cluster member
        {
            for(position=0;position<MAX_ACTIVE;position++)
            {
                for(shift=0;shift<8;shift++)
                {
                    // Check if the neighbouring CH's are neighbours of the other node as well
                    if(((root_list[position] >> shift)& 0x01) & ((syncMsg.gossip_list[position] >> shift) & 0x01))
                    {
                        if(syncMsg.id < detached_needed) // it's ok for this node to not break out
                        {
                            detached_needed = syncMsg.id;
                            printf("Detached node will be %d\n", detached_needed); 
                        }
                    }
                }
            }
        }


        /* Perform the correction */
        if((syncMsg.id == root_id) && !detached_root)
        {
            for(position=0;position<MAX_ACTIVE;position++)
            {
                for(shift=0;shift<8;shift++)
                {
                    if(((root_list[position] >> shift) & 0x01) && ((position*8+shift) != syncMsg.id) && \
                        !(((root_list[position] >> shift) & (syncMsg.gossip_list[position] >> shift)) & 0x01))
                    {
                        if(detached_needed == node_id)
                        {
                            root_id = node_id;
                            detached_root = 1;
                            fixed_root = 0;  
                            printf("This node has to update its status due to msg from %d for %d\n", syncMsg.id, (position*8+shift));
                        }
                    }

                }
            }
        }

        break;

        case HEALTH_UPDATE:


        position = syncMsg.id / 8;
        shift = syncMsg.id % 8;
        if((syncMsg.id == syncMsg.root) && (node_id == root_id))
        {
            root_list[position] = root_list[position] | (1U << (shift));
            printf("CH neighbour of %d is %d\n", node_id, syncMsg.id);
        }
        /* Initially every node will be a root of its own */
        else
        {
            root_list[position] = root_list[position] & ~(1U << (shift));
        }

        /* Error Correction: Node claimed itself to be root and later lost its status */
        if((root_id == syncMsg.id) && (syncMsg.id != syncMsg.root))
        {
            root_list[position] = root_list[position] & ~(1U << (shift));
            if(highest_node != syncMsg.id)
            {
                position = highest_node / 8;
                shift = highest_node % 8;
                if((root_list[position] >> shift) & 0x01)
                {
                    root_id = highest_node;
                }
                else
                {
                    root_id = node_id;
                }
            }
            else if(cluster_neighbours < shighest_degree)
            {
                position = shighest_node / 8;
                shift = shighest_node % 8;
                if((root_list[position] >> shift) & 0x01)
                {
                    root_id = shighest_node;
                }
                else
                {
                    root_id = node_id;
                }
            }
            else
            {
                root_id = node_id;
            }
            printf("Updating CH\n");
        }

        verify_neighbour_corr(&syncMsg);
        break;

        default:
        break;
    }
}


void verify_neighbour_corr(struct message_t* msg)
{
    uint8_t i = 0, j = 0, correction = 0, temp = 0;
    struct neighbour *n;
    for(i = 0; i < MAX_ACTIVE; i++)
    {
        for(j=0; j < 8; j++)
        {
            if(!((fixed_neighbours[i] >> j) & 0x01))
                continue;
            else if(((fixed_neighbours[i] >> j) & 0x01) ^ \
                 ((gossip_list[i] >> j) & 0x01))
            {
                correction = (((gossip_list[i] >> j) & 0x01)  ^ ((msg->gossip_list[i] >> j) & 0x01));
                if(correction)//(gossip_list[i] >> j) & 0x01)
                { 
                    temp = (i*8)+j;
                    for(n = list_head(neighbour_list); n != NULL; n = list_item_next(n)) 
                    {
                        if((n->id == temp))// && (root_id != temp))
                        {
                            if((n->id == root_id) && (!root_gossip))
                            {
                                n->reset = n->reset/2;
                                if(n->reset > (NODE_FAIL_LIMIT))
                                    gossip_list[i] = gossip_list[i] & ~(1U << (j));
                                root_gossip = 1;
                            }
                            else if(n->id != root_id)
                            {
                                n->reset = n->reset/2;
                                if(n->reset > (NODE_FAIL_LIMIT * CYCLE_LIMIT))
                                    gossip_list[i] = gossip_list[i] & ~(1U << (j));

                            }
                        }

                    }
                }
            }
        }       
    }
}


void add_to_neighbour(uint16_t msg_id,uint16_t msg_root, uint8_t ext, int16_t ext_neighb, uint8_t remove)
{
    struct neighbour *n;
    uint8_t position = 0, shift = 0;
    /* Check if we already know this neighbor. */
    for(n = list_head(neighbour_list); n != NULL; n = list_item_next(n)) 
    {
        if(n->id == msg_id)
        {
            if((remove))
            {
                if(ext_neighb)
                    external--;
                list_remove(neighbour_list, n);
                memb_free(&neighbour_memb, n);
                cluster_neighbours--;
            }
			
            n->reset = 0;
            return;
        }
        else if(msg_id == 0)
        {
            n->reset++;
            position = n->id / 8;
            shift = n->id % 8;
            if((n->id == root_id) && (n->reset > NODE_FAIL_LIMIT))
                gossip_list[position] = gossip_list[position] & ~(1U << (shift));
            else if((n->id != root_id) && (n->reset > (NODE_FAIL_LIMIT * CYCLE_LIMIT)))
            {
                //printf("Removing candidate node %ld\n", n->id);
                gossip_list[position] = gossip_list[position] & ~(1U << (shift));
            }
            if((n->id == root_id) && (n->reset > 2 * NODE_FAIL_LIMIT))
            {
                cluster_neighbours--;
                if((cluster_neighbours >= (shighest_degree - 1)) && (node_id < shighest_node))
                {
                    root_id = node_id;
                    highest_degree = cluster_neighbours;
                    highest_node = root_id;
                }
                else if(shighest_node > 0)
                {
                    position = shighest_node / 8;
                    shift = shighest_node % 8;
                    if((root_list[position] >> shift) & 0x01)
                    {
                        root_id = shighest_node;
                        highest_degree = shighest_degree;
                        highest_node = root_id;
                        shighest_node = 0;
                        shighest_degree = 0;
                    }
                    else
                    {
                        root_id = node_id;
                        highest_degree = cluster_neighbours;
                        highest_node = root_id;
                    }
                    
                }
                position = n->id / 8;
                shift = n->id % 8;
                root_list[position] = root_list[position] & ~(1U << (shift));
                printf("Root is dead! time for new root %d\n", root_id);
                mode = CH_ELECTION;
                printf("Changing to CH_ELECTION state\n");
                /* Start the election immediately */ 
                sub_round = 2;
                gossip_list[position] = gossip_list[position] & ~(1U << (shift));
                list_remove(neighbour_list, n);
                memb_free(&neighbour_memb, n);
            }
            else if((n->id != root_id) && (n->reset > (2 * NODE_FAIL_LIMIT * CYCLE_LIMIT)))
            {
                //printf("Removing node %d as dead\n", n->id);
                cluster_neighbours--;
                if(ext_neighb)
                    external--;
                if(n->id == shighest_node)
                {
                    shighest_node = 0;
                    shighest_degree = 0;
                }
                /* In case the bridge node is dead, the potential bridge take over */
                else if(n->id == detached_needed)
                {
                    root_id = node_id;
                    detached_needed = node_id;
                    mode = CLUSTER_CORRECTION;
                    printf("Changing to CLUSTER_CORRECTION state\n");
                }

                if((cluster_members[position] >> shift) & 0x01)
                {
                   cluster_members[position] = cluster_members[position] & ~(1U << (shift));
                   //printf("Removing a cluster member\n");
                }
                gossip_list[position] = gossip_list[position] & ~(1U << (shift));
                root_list[position] = root_list[position] & ~(1U << (shift));
                fixed_neighbours[position] = fixed_neighbours[position] & ~(1U << (shift));
                list_remove(neighbour_list, n);
                memb_free(&neighbour_memb, n);
                
            }

        }
    }
    if(msg_id == 0)
        return;

    if(n == NULL) 
    {
        n = memb_alloc(&neighbour_memb);

        /* If we could not allocate a new neighbor entry, we give up */
        if(n == NULL) 
          return;
        n->id = msg_id;
        n->root = msg_root;
        if(ext)
        { 
            external++; 
        }
        cluster_neighbours++;
        n->reset = 0;
        list_add(neighbour_list, n);
        {
            position = n->id / 8;
            shift = n->id % 8;
            gossip_list[position] = gossip_list[position] | (1U << (shift));
            fixed_neighbours[position] = fixed_neighbours[position] | (1U << (shift));
        }
    }
}

static void state_machine()
{
    // Isolated node without any neighbours
    if(!cluster_neighbours)// && (mode == NEIGHBOUR_DISCOVERY)) (cluster_neighbours > highest_degree) ||
    {
        root_id = node_id;
        fixed_root = 0;
    }
    
    sub_round++;

    if(!(sub_round % ROUND_LIMIT))
    {
        if(rounds < 100) 
            rounds++;
    }
    /* To prevent overflow */
    else if(sub_round >100)
        sub_round = 100;


    if(!(rounds % CYCLE_LIMIT))
    {
        random_tx = (abs(rand()) % CYCLE_LIMIT);
    }
    /* 1 sub round is needed for the node start up */
    if(!(sub_round % (1 + ROUND_LIMIT)) && (mode == NEIGHBOUR_DISCOVERY))
    {
        mode = CH_ELECTION;
        neighbour_phase = 1;
        printf("Changing to CH_ELECTION state\n");
    }
    else if((!(sub_round % (2 + ROUND_LIMIT)) && (mode == CH_ELECTION)))
    {
        mode = CLUSTER_CORRECTION;
        printf("Changing to CLUSTER_CORRECTION state\n");
        neighbour_phase = 1;
    }
    else if((!(sub_round % (3 + ROUND_LIMIT)) && (mode == CLUSTER_CORRECTION)))
    {
        mode = HEALTH_UPDATE;
        printf("Changing to HEALTH_UPDATE state\n");
        powertrace_print("");
        neighbour_phase = 0;
    }

    
    if(!(rounds % CYCLE_LIMIT) || (mode!=HEALTH_UPDATE))
        printf("Cluster head for node %d is node %d\n", node_id, root_id);

    if((mode == HEALTH_UPDATE) && !(rounds % EPOCH))
        powertrace_print("");

}



static const struct broadcast_callbacks broadcast_call = {broadcast_recv};
static struct broadcast_conn broadcast;
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(decoric_process, ev, data)
{
  static struct etimer sendtimer;
  static clock_time_t interval;
  struct message_t beacon;

  #if 1
  /* Variables for residual energy */
  uint16_t seconds=10;// warning: if this variable is changed, then the kinect variable the count the minutes should be changed
  double fixed_perc_energy = 0.002;// 0 - 1
  uint16_t variation = 2;//0 - 99
  #endif

  static uint16_t residual = 0;

  PROCESS_EXITHANDLER(broadcast_close(&broadcast);)

  PROCESS_BEGIN();

  /* Initialize the memory for the neighbour entries. */
  memb_init(&neighbour_memb);
  list_init(neighbour_list);

  powertrace_start(CLOCK_SECOND * seconds, seconds, fixed_perc_energy, variation);
  //powertrace_start(CLOCK_SECOND * 10);

  broadcast_open(&broadcast, XFER_CHANNEL, &broadcast_call);
  // node_id_burn(node_id); // To be used for hardware

  interval = MIN_INTERVAL;

  while(1)
  {
    etimer_set(&sendtimer, interval);

    PROCESS_WAIT_UNTIL(etimer_expired(&sendtimer));

    residual = powertrace_getresidual();

    if(!residual)
    {
        printf("Residual energy of node %d is 0\n", node_id);
        break;
    }

    /* Start alive counter after health update */
    if((mode == HEALTH_UPDATE) && (!(sub_round % ROUND_LIMIT)))
        add_to_neighbour(FALSE, FALSE, FALSE, FALSE, FALSE);


    /* Update the state machine */
    state_machine();

    /* Send the actual packet */
    beacon.dispatch_marker = PROTOCOL_DISPATCH;
    beacon.id = node_id;
    beacon.root = root_id;
    beacon.new_root = new_root;
    beacon.neighbours = cluster_neighbours;
    memcpy(beacon.gossip_list, gossip_list, sizeof(gossip_list));


    if((node_id != root_id) && (mode == HEALTH_UPDATE))
    {
        if(rounds == random_tx)
        {
            beacon.id = node_id;
        }
        else
            continue;
        
    }
    else if((node_id == root_id) && ((mode == HEALTH_UPDATE)))
    {
        if(!(sub_round % ROUND_LIMIT))
            beacon.id = node_id;
        else
            continue;
    }

    /* Send the packet */
    packetbuf_copyfrom(&beacon, sizeof(beacon));
    broadcast_send(&broadcast);

    PROCESS_END();
  }
  return 1;
}
