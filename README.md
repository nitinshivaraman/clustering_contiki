The Contiki Operating System
============================

[![Build Status](https://secure.travis-ci.org/contiki-os/contiki.png)](http://travis-ci.org/contiki-os/contiki)

Contiki is an open source operating system that runs on tiny low-power
microcontrollers and makes it possible to develop applications that
make efficient use of the hardware while providing standardized
low-power wireless communication for a range of hardware platforms.

Contiki is used in numerous commercial and non-commercial systems,
such as city sound monitoring, street lights, networked electrical
power meters, industrial monitoring, radiation monitoring,
construction site monitoring, alarm systems, remote house monitoring,
and so on.

For more information, see the Contiki website:

[http://contiki-os.org](http://contiki-os.org)



# Clustering Protocols in Contiki -- DeCoRIC, BEEM and LEACH

This repository provides the source code for 3 Clustering protocols.
The source files for the protocols are under examples/clustering

The folders 50_100m, 100_100m and 200_100m contain the topology of 100 different networks used for the experiments.

Note: The TDMA protocol in LEACH and BEEM are implemented through an event mechanism from the protocol source file using a non-static variable.

## Variables and files to update for running the protocols:
### Source files (*.c)
The "ROUND_TIME" variable depending on the number of nodes in the network. This is set based on the maximum number of nodes.


### Configuration
DeCoRIC uses CXMAC while BEEM and LEACH use NULLMAC (under NETSTACK_CONF_RDC)

### Build
Makefile needs to be modified to include the corresponding source (under all option). Command "make TARGET=sky all"